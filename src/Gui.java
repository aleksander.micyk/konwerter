import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Klasa odpowiedzialna za wygląd aplikacji.
 */
public class Gui extends JFrame {

    private static final long serialVersionUID = 1L;
    private final JLabel lblVariable;
    private JLabel lblScore;
    private JTextField textFieldVariable;
    private JTextField textFieldScore;
    private JComboBox<?> comboBox;


    /**
     * Konstruktor umozliwiajacy tworzenie okienkowego programu.
     * Wygenerowanie wszystkich przycisków i pól tekstowych aplikacji.
     */
    Gui() {
        setType( Type.UTILITY );
        setTitle( "Basic Units Converter" );

        int width = Toolkit.getDefaultToolkit().getScreenSize().width; // pobieranie zestawu narzedzi i przypisanie do zmniennej szer
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;

        this.setSize( 460, 537 );

        int widthFrame = this.getSize().width;
        int heightFrame = this.getSize().height;


        setResizable( false );
        setBounds( ((width - widthFrame) / 2), (height - heightFrame) / 2, 460, 300 );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        getContentPane().setLayout( null );

        JLabel lblChooseOptions = new JLabel( "choose_option" );
        lblChooseOptions.setBounds( 28, 11, 375, 34 );
        getContentPane().add( lblChooseOptions );

        textFieldVariable = new JTextField();
        textFieldVariable.setDocument( new JTextFieldLimit( 6 ) );  // wywołanie klasy odpowiedzilanej za limit znaków w polu
        textFieldVariable.setBounds( 28, 184, 152, 30 );

        textFieldVariable.addKeyListener( new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {
                String value = textFieldVariable.getText();
                int l = value.length();
                if (ke.getKeyChar() >= '0' && ke.getKeyChar() <= '9' || ke.getKeyChar() == KeyEvent.VK_BACK_SPACE || ke.getKeyChar() == '.') {
                    textFieldVariable.setEditable( true );
                } else {
                    textFieldVariable.setEditable( false );
                }
            }
        } );
        textFieldVariable.setText( "1" );
        getContentPane().add( textFieldVariable );
        textFieldVariable.setColumns( 10 );
        textFieldVariable.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                try {
                    Menu menu = new Menu();
                    textFieldScore.setText( menu.menu( comboBox.getSelectedIndex(), Double.parseDouble( textFieldVariable.getText() ) ) );
                } catch (Exception e1) {
                    textFieldScore.setText( "Bad record" );
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (textFieldVariable.getText().length() < 1) {
                    textFieldScore.setText( "" );
                } else {
                    try {
                        Menu menu = new Menu();
                        textFieldScore.setText( menu.menu( comboBox.getSelectedIndex(), Double.parseDouble( textFieldVariable.getText() ) ) );
                    } catch (Exception e1) {
                        textFieldScore.setText( "Bad record" );
                    }
                }

            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                textFieldScore.setText( "" );
            }
        } );

        textFieldScore = new JTextField();
        textFieldScore.setText( "0,22" );
        textFieldScore.setEditable( false );
        textFieldScore.setBounds( 251, 184, 152, 30 );
        getContentPane().add( textFieldScore );

        textFieldScore.setColumns( 10 );

        JLabel lblNewLabel = new JLabel( "<==>" );
        lblNewLabel.setHorizontalAlignment( SwingConstants.CENTER );
        lblNewLabel.setBounds( 190, 184, 51, 30 );
        getContentPane().add( lblNewLabel );

        lblVariable = new JLabel( "PLN" );
        lblVariable.setHorizontalAlignment( SwingConstants.LEFT );
        lblVariable.setBounds( 28, 159, 152, 14 );
        getContentPane().add( lblVariable );

        lblScore = new JLabel( "EURO" );
        lblScore.setHorizontalAlignment( SwingConstants.RIGHT );
        lblScore.setBounds( 251, 159, 152, 14 );
        getContentPane().add( lblScore );


        String[] comboBoxMenu =
                {"PLN --> EURO", "EURO --> PLN",
                        "Square meters --> Square inches", "Square inches > Square meters",
                        "Kilograms --> Ounces", "Ounces --> Kilograms"};

        JComboBox jComboBox = new JComboBox( comboBoxMenu );
        comboBox = jComboBox;
        comboBox.setBounds( 28, 84, 383, 47 );
        comboBox.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (comboBox.getSelectedIndex()) {
                    case 0:
                        lblVariable.setText( "PLN" );
                        lblScore.setText( "EURO" );
                        break;
                    case 1:
                        lblVariable.setText( "EURO" );
                        lblScore.setText( "PLN" );
                        break;
                    case 2:
                        lblVariable.setText( "Square meters" );
                        lblScore.setText( "Square inches" );
                        break;
                    case 3:
                        lblVariable.setText( "Square inches" );
                        lblScore.setText( "Square meters" );
                        break;
                    case 4:
                        lblVariable.setText( "Kilograms" );
                        lblScore.setText( "Ounces" );
                        break;
                    case 5:
                        lblVariable.setText( "Ounces" );
                        lblScore.setText( "Kilograms" );
                        break;

                    default:
                }
            }
        } );
        getContentPane().add( comboBox );

    }

    /**
     * Metoda wyświetlająca Gui aplikacji
     */
    void showApp() {
        EventQueue.invokeLater( new Runnable() {
            public void run() {
                try {
                    setVisible( true );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } );
    }


}


