/**
 * @author Aleksander Micyk
 * Sem 5. studia niestacjonarne Informatyka
 * @version 1.0.0 15.11.2020
 * <p>
 * <p>
 * Konwerter jednostek miar - wersja okienkowa.
 */


public class Konwerter {

    public static void main(String[] args) {

        //Wywołanie klasy odpowiadającej za tworzenie i wyświetlenie programu okienkowego z możliwością wczytania danych z pliku i zapisem.
        Gui gui = new Gui();
        gui.showApp();

    }


}
